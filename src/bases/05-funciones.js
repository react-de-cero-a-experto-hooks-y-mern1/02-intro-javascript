// Funciones en JS
const saludar = function (nombre) {
  return `Hola, ${nombre}`;
};
//saludar = 20;

const saludar2 = (nombre) => {
  return `Hola, ${nombre}`;
};

const saludar3 = (nombre) => `Hola, ${nombre}`;
const saludar4 = () => `Hola Mundo`;
console.log(saludar('Goku'));
//console.log(saludar);
console.log(saludar2('Vegeta'));
console.log(saludar3('Vegeta'));
console.log(saludar4());

const getUser = () => ({
  uid: 'ABC123',
  username: 'user_uno',
});
console.log(getUser());

// Tarea
// 1. Transformar a una funcion de flecha
// 2. Retornar un objeto implicito
// 3. Pruebas
const getUsuarioActivo = (nombre) => ({
  uid: 'ABC421',
  username: nombre,
});

const usuarioActivo = getUsuarioActivo('Melisa');
console.log(usuarioActivo);
